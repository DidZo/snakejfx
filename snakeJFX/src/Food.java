public class Food {
	Segment location;

	public Food(int x, int y) {
		location = new Segment(x, y);
	}

	public Segment getLocation() {
		return location;
	}

	@Override
	public String toString() {
		return "Food [myLocation=" + location + "]";
	}

}
