import java.util.Timer;
import java.util.TimerTask;

import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class SnakeGame extends Application {

	public static void main(String[] args) {
		Application.launch(args);
	}

	private static final long TASK_UPDATE_PERIOD_MS = 10;
	private static final long TASK_UPDATE_DELAY_MS = TASK_UPDATE_PERIOD_MS;

	private static final int WINDOW_HEIGHT = 800;
	private static final int WINDOW_WIDTH = 800;
	private static final int GRID_BLOCK_SIZE = 50;
	private GraphicsContext graphicsContext;
	private Button startButton;
	private Snake snake1;
//	private Snake snake2;
	private Grid grid;
	private AnimationTimer animationTimer;
	private Timer timer;
	private TimerTask task;
	private Direction nextSnakeDirection1 = Direction.RIGHT;
	private Direction currentSnakeDirection1 = Direction.RIGHT;
	private Direction nextSnakeDirection2 = Direction.DOWN;
	private Direction currentSnakeDirection2 = Direction.DOWN;

	private int threadCompteur = 0;
	private boolean isGameInProgress = false;
	private boolean isGameOver = false;
	private boolean isPaused = false;

	@Override
	public void start(Stage primaryStage) throws Exception {

		primaryStage.setTitle("Snake");


		Group root = new Group();
		Canvas canvas = new Canvas(WINDOW_WIDTH, WINDOW_HEIGHT);
		graphicsContext = canvas.getGraphicsContext2D();
		root.getChildren().add(canvas);
		Scene scene = new Scene(root);

		grid = new Grid(WINDOW_WIDTH, WINDOW_HEIGHT, GRID_BLOCK_SIZE);
		snake1 = new Snake(WINDOW_WIDTH, WINDOW_HEIGHT, GRID_BLOCK_SIZE);
		snake1.setHeadLocation(GRID_BLOCK_SIZE, GRID_BLOCK_SIZE);

//		snake2 = new Snake(WINDOW_WIDTH, WINDOW_HEIGHT, GRID_BLOCK_SIZE);
//		snake2.setHeadLocation(GRID_BLOCK_SIZE*2, GRID_BLOCK_SIZE*2);


		drawGrid();

		startButton = new Button("Start!");
		startButton.setMinWidth(100);
		startButton.setMinHeight(36);

		VBox vBox = new VBox();
		vBox.prefWidthProperty().bind(canvas.widthProperty());
		vBox.prefHeightProperty().bind(canvas.heightProperty());
		vBox.setAlignment(Pos.CENTER);
		vBox.getChildren().add(startButton);


//		root.setStyle("-fx-background-image: url('fond_snake.png'); " +
//				"-fx-background-position: center center; " +
//				"-fx-background-repeat: stretch;");

		startButton.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				isGameInProgress = true;
				isGameOver = false;
				startButton.setVisible(false);

				if (timer == null) {
					task = createTimerTask();
					timer = new Timer("Timer");
					timer.scheduleAtFixedRate(task, TASK_UPDATE_DELAY_MS, TASK_UPDATE_PERIOD_MS);
					animationTimer.start();
				}
			}
		});

		root.getChildren().add(vBox);

		scene.setOnKeyPressed((e) -> {
			if (e.getCode() == KeyCode.Z) {
				if(currentSnakeDirection1!= Direction.DOWN) {
					nextSnakeDirection1 = Direction.UP;
				}
			} else if (e.getCode() == KeyCode.S) {
				if(currentSnakeDirection1!= Direction.UP) {
					nextSnakeDirection1 = Direction.DOWN;
				}
			} else if (e.getCode() == KeyCode.Q) {
				if(currentSnakeDirection1!= Direction.RIGHT) {
					nextSnakeDirection1 = Direction.LEFT;
				}
			} else if (e.getCode() == KeyCode.D) {
				if(currentSnakeDirection1!= Direction.LEFT) {
					nextSnakeDirection1 = Direction.RIGHT;
				}
			}else if (e.getCode() == KeyCode.UP) {
				if(currentSnakeDirection2!= Direction.DOWN) {
					nextSnakeDirection2 = Direction.UP;
				}
			} else if (e.getCode() == KeyCode.DOWN) {
				if(currentSnakeDirection2!= Direction.UP) {
					nextSnakeDirection2 = Direction.DOWN;
				}
			} else if (e.getCode() == KeyCode.LEFT) {
				if(currentSnakeDirection2!= Direction.RIGHT) {
					nextSnakeDirection2 = Direction.LEFT;
				}
			} else if (e.getCode() == KeyCode.RIGHT) {
				if(currentSnakeDirection2!= Direction.LEFT) {
					nextSnakeDirection2 = Direction.RIGHT;
				}
			} else if (e.getCode() == KeyCode.P) {

				if (isPaused) {
					task = createTimerTask();
					timer = new Timer("Timer");
					timer.scheduleAtFixedRate(task, TASK_UPDATE_DELAY_MS, TASK_UPDATE_PERIOD_MS);
					isPaused = false;
				} else {
					timer.cancel();
					isPaused = true;
				}
			}

		});



		primaryStage.setScene(scene);
		primaryStage.show();

		animationTimer = new AnimationTimer() {
			@Override
			public void handle(long timestamp) {
				if (isGameInProgress) {
					drawGrid();
					drawSnake();
					drawFood();
				} else if (isGameOver) {
					animationTimer.stop();
					showEndGameAlert();
					startButton.setVisible(true);
					grid.reset();
					snake1 = new Snake(WINDOW_WIDTH, WINDOW_HEIGHT, GRID_BLOCK_SIZE);
					snake1.setHeadLocation(GRID_BLOCK_SIZE, GRID_BLOCK_SIZE);
					currentSnakeDirection1 = Direction.RIGHT;
					nextSnakeDirection1 = Direction.RIGHT;
//
//					snake2 = new Snake(WINDOW_WIDTH, WINDOW_HEIGHT, GRID_BLOCK_SIZE);
//					snake2.setHeadLocation(GRID_BLOCK_SIZE*2, GRID_BLOCK_SIZE*2);
//					currentSnakeDirection2 = Direction.DOWN;
//					nextSnakeDirection2 = Direction.DOWN;

					threadCompteur=0;

				}
			}
		};
		animationTimer.start();

		task = createTimerTask();
		timer = new Timer("Timer");
		timer.scheduleAtFixedRate(task, TASK_UPDATE_DELAY_MS, TASK_UPDATE_PERIOD_MS);
	}

	@Override
	public void stop() {
		if (timer != null) {
			timer.cancel();
		}
	}

	private TimerTask createTimerTask() {
		TimerTask task = new TimerTask() {
			@Override
			public void run() {
				if (isGameInProgress) {
					if(threadCompteur % 10 == 0){
						currentSnakeDirection1 = nextSnakeDirection1;
						currentSnakeDirection2 = nextSnakeDirection2;
					}
					snake1.setDirection(currentSnakeDirection1);
//					snake2.setDirection(currentSnakeDirection2);


					snake1.snakeUpdateNormal();
//					snake2.snakeUpdateNormal();
					threadCompteur++;

					if (snake1.collidedWithWall()) {
						endGame("collided with wall");
					} else if (snake1.collidedWithTail()) {
						endGame("collided with tail");
					}
//					if (snake2.collidedWithWall()) {
//						endGame("collided with wall");
//					} else if (snake2.collidedWithTail()) {
//						endGame("collided with tail");
//					}

					boolean foundFood1 = grid.foundFood(snake1);
//					boolean foundFood2 = grid.foundFood(snake2);

					if (foundFood1) {
						snake1.addPointToPrint();
						grid.addFood();
						System.out.println("Snake length: " + snake1.getTail().size());
					}
//					if (foundFood2) {
//						snake2.addPointToPrint();
//						grid.addFood();
//						System.out.println("Snake length: " + snake2.getTail().size());
//					}
				}
			}
		};
		return task;
	}

	private void endGame(String reason) {
		timer.cancel();
		timer = null;
		isGameInProgress = false;
		isGameOver = true;
		System.out.println("Game over: " + reason);
	}

	private void showEndGameAlert() {
		String gameOverText = "Game Over! Score: " + (snake1.getTail().size() + 1);
		double textWidth = getTextWidth(gameOverText);

		graphicsContext.setFill(Color.BLACK);
		graphicsContext.fillText(gameOverText, (WINDOW_WIDTH / 2) - (textWidth / 2), WINDOW_HEIGHT / 2 - 24);
	}

	private void drawGrid() {
//		Image image = new Image(getClass()
//				.getResourceAsStream("fond_snake.png"));
//
//
//		ImageView imageView = new ImageView(image);
//
//		imageView.setX(0);
//		imageView.setY(0);
//
//		imageView.setFitHeight(800);
//		imageView.setFitWidth(800);
//		imageView.
//
//		root.getChildren().add(imageView);

//
//		graphicsContext.setFill(Color.TRANSPARENT);
//		graphicsContext.fillRect(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT);
//		graphicsContext.setStroke(Color.LIGHTGRAY);
//		graphicsContext.setLineWidth(0.5);
//
//		for (int x = 0; x < WINDOW_WIDTH; x += GRID_BLOCK_SIZE) {
//			graphicsContext.strokeLine(x, 0, x, x + WINDOW_HEIGHT);
//		}
//
//		for (int y = 0; y < WINDOW_HEIGHT; y += GRID_BLOCK_SIZE) {
//			graphicsContext.strokeLine(0, y, y + WINDOW_WIDTH, y);
//		}
	}

	private void drawSnake() {

		graphicsContext.clearRect(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT);
		graphicsContext.setFill(Color.PURPLE);
		graphicsContext.fillRect(snake1.getHeadLocation().getX(), snake1.getHeadLocation().getY(), snake1.getBlockSize(),
				snake1.getBlockSize());

		for (Segment tailSegment : snake1.getTail()) {
			graphicsContext.fillRect(tailSegment.getX(), tailSegment.getY(), snake1.getBlockSize(),
					snake1.getBlockSize());
		}
//		graphicsContext.setFill(Color.YELLOW);
//		graphicsContext.fillRect(snake2.getHeadLocation().getX(), snake2.getHeadLocation().getY(), snake2.getBlockSize(),
//				snake2.getBlockSize());
//
//		for (Segment tailSegment : snake2.getTail()) {
//			graphicsContext.fillRect(tailSegment.getX(), tailSegment.getY(), snake2.getBlockSize(),
//					snake2.getBlockSize());
//		}
	}

	private void drawFood() {
		graphicsContext.setFill(Color.RED);
		graphicsContext.fillRect(grid.getFood().getLocation().getX(), grid.getFood().getLocation().getY(),
				GRID_BLOCK_SIZE, GRID_BLOCK_SIZE);
	}

	private double getTextWidth(String string) {
		Text text = new Text(string);
		new Scene(new Group(text));
		text.applyCss();
		return text.getLayoutBounds().getWidth();
	}

}
