
import java.util.ArrayList;
import java.util.List;

public class Snake {

	private Direction direction;
	private Segment headLocation = new Segment(0, 0);
	private List<Segment> tail = new ArrayList<Segment>();
	private List<Segment> posHead = new ArrayList<>();
	private int height;
	private int width;
	private int blockSize;
	private boolean isCollidedWithWall = false;
	private int toPrint = 0;

	public Snake(int width, int height, int blockSize) {
		this.width = width;
		this.height = height;
		this.blockSize = blockSize;
		this.direction = Direction.RIGHT;
	}

	public void snakeUpdateNormal() {

		if(toPrint>0){
			addTailSegment();
		}
		this.posHead.add(0,new Segment(headLocation.getX(),headLocation.getY()));
		System.out.println(posHead);
		for (int i = 0; i < tail.size() ; i++) {
			deplacementSimple(posHead.get(i),tail.get(i));
		}
//		if(posHead.size()>tail.size()*10){
//			for (int i = tail.size()*10+1; i <posHead.size(); i++) {
//				posHead.remove(i);
//			}
//		}
		if(posHead.size()>tail.size()*10) {
			posHead.subList(tail.size() * 10, posHead.size()).clear();
		}
		switch (direction) {

		case UP:
			headLocation.setY(headLocation.getY() - blockSize/10);
			if (headLocation.getY() < 0) {
				isCollidedWithWall = true;
				headLocation.setY(0);
			}
			break;

		case DOWN:
			headLocation.setY(headLocation.getY() + blockSize/10);
			if (headLocation.getY() >= height) {
				isCollidedWithWall = true;
				headLocation.setY(height);
			}
			break;

		case LEFT:
			headLocation.setX(headLocation.getX() - blockSize/10);
			if (headLocation.getX() < 0) {
				isCollidedWithWall = true;
				headLocation.setX(0);
			}
			break;

		case RIGHT:
			headLocation.setX(headLocation.getX() + blockSize/10);
			if (headLocation.getX() >= width) {
				isCollidedWithWall = true;
				headLocation.setX(width - blockSize);
			}
			break;

		default:
			break;
		}
		System.out.println(headLocation);

	}


	public boolean collidedWithWall() {
		return isCollidedWithWall;
	}

	public boolean collidedWithTail() {
		boolean isCollision = false;

		for (Segment tailSegment : tail) {
			if (headLocation.equals(tailSegment)) {
				isCollision = true;
				break;
			}
		}

		return isCollision;
	}

	public void addTailSegment(){
		tail.add(new Segment(headLocation.getX(), headLocation.getY()));
		toPrint--;
	}

	public void setDirection(Direction myDirection) {

		this.direction = myDirection;

	}

	public int getBlockSize() {
		return blockSize;
	}

	public void setHeadLocation(int x, int y) {
		headLocation.setX(x);
		headLocation.setY(y);
	}

	public Segment getHeadLocation() {
		return headLocation;
	}

	public List<Segment> getTail() {
		return tail;
	}


	public void deplacementSimple(Segment pos, Segment segment){
		segment.setX(pos.getX());
		segment.setY(pos.getY());
	}

	public void addPointToPrint(){
		toPrint += 10;
	}
}
